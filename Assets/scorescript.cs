﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scorescript : MonoBehaviour {
    public static int score;
    public Text stext;
	// Use this for initialization
	void Start () {
        score = 0;
        stext.text = "Your Score is: " + score;
    }
	
	// Update is called once per frame
	void Update () {
    }
}
