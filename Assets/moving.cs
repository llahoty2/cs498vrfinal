﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moving : MonoBehaviour {
    public float speed = 20f;
    public Camera C;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if ((OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch)[1]) > 0) {

            this.transform.position += C.transform.forward * Time.deltaTime * speed;
        }
        if ((OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch)[1]) < 0) { 
            this.transform.position += Vector3.forward * Time.deltaTime * speed;
        }
        if ((OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick, OVRInput.Controller.RTouch)[1]) > 0)
        {
            this.transform.position += Vector3.right * Time.deltaTime * speed;
        }
        if ((OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick, OVRInput.Controller.RTouch)[1]) < 0)
        {
            this.transform.position += Vector3.left * Time.deltaTime * speed;
        }
    }
}
