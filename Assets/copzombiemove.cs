﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class copzombiemove : MonoBehaviour {

    NavMeshAgent nav;
    public Camera C;
    public GameObject gun;
    public Animator anim;
    Vector2 smoothDeltaPosition = Vector2.zero;
    Vector2 velocity = Vector2.zero;
    public GameObject[] dests;
    int spawnIndex;
    bool setdestination = false;
    RaycastHit hit = new RaycastHit();
	// Use this for initialization
	void Start () {
        nav = GetComponent<NavMeshAgent>();



        //nav.updatePosition = false;
       // transform.LookAt(gun.transform);
         spawnIndex = Random.Range(0, 3);

    }
	
	// Update is called once per frame
	void Update () {
        //
       

        this.transform.LookAt(nav.steeringTarget + transform.forward);

        if (Mathf.Abs(this.transform.position.z - gun.transform.position.z) < 3.0 )
        {
            anim.SetBool("attack", true);
            //nav.updatePosition = false;
            nav.isStopped = true;
            //this.transform.LookAt(gun.transform);
        }
        else if(!setdestination)
        {
            Debug.Log(spawnIndex);
            nav.SetDestination(dests[spawnIndex].transform.position);
            setdestination = true;
          
        }
        
       // this.transform.rotation = C.transform.rotation;
    }
}
