﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet2 : MonoBehaviour {
    public Rigidbody bullet_object_2;
    float bulletspeed = 100f;
    public AudioSource source;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            source.Play();
            Rigidbody Bullet2;
            Bullet2 = Instantiate(bullet_object_2, transform.position, transform.rotation);
            Bullet2.gameObject.AddComponent<bulletspeed>();

        }
    }
}
