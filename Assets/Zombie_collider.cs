﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zombie_collider : MonoBehaviour
{
    public Text scoreText;
    private static int score;
    public Animator anim;
    void Start()
    {
        scoreText.text = "score:" + score;
        //score = scorescript.score;
        // target = Camera.transform;
    }
    void Update()
    {

    }
    void OnTriggerEnter(Collider other) { 
        Debug.Log("Collision");
        if (other.gameObject.tag == "bull")
        {
            score += 1;
            // gameObject.SetActive(false);
            
            anim.SetBool("shot", true);
            Destroy(this.gameObject, anim.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + 2f);

            Debug.Log(score);
        }
        scoreText.text = "Your Score is " + score;
    }
}
