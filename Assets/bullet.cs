﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {
    public Rigidbody bullet_object;
    float bulletspeed = 100f;
    public AudioSource source;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

       
       //this.transform.rotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            source.Play();
            Rigidbody Bullet;
            Bullet = Instantiate(bullet_object, transform.position, transform.rotation);
            Bullet.gameObject.AddComponent<bulletspeed>();

        }
       
    }
}
