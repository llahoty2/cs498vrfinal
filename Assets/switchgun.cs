﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchgun : MonoBehaviour {
    public GameObject g1;
    public GameObject g2;
	// Use this for initialization
	void Start () {
        
        //g1.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            SelectWeapon();
        }
    }

    void SelectWeapon()
    {
        if (g1.activeSelf)
        {
            g1.SetActive(false);
            g2.SetActive(true); 
        }
        else
        {
            g1.SetActive(true);
            g2.SetActive(false);
        }
    }
}
