﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class alien_collider : MonoBehaviour {
    //public GameObject Camera;
    //public Animator anim; 
    public Text scoreText;
    private static int score;

// Use this for initialization
void Start () {
    scoreText.text = "score:" + score;
}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision");
        if (other.gameObject.tag == "bear")
        {
            score += 1;
            gameObject.SetActive(false);

            //anim.SetBool("isdead",true);
           //Destroy(this.gameObject, anim.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + 2f);
            Debug.Log(score);
        }
        scoreText.text = "Your Score is " + score;
    }
}
